#!/bin/sh
root=$(dirname $(dirname $(realpath "$0")))

# mark.js
cd "$root/build/mark.js" &&
npm update &&
cp node_modules/mark.js/dist/mark.es6.js "$root/themes/default/static/_common/lib/"

# microformats-parser
cd "$root/build/mf2" &&
npm update &&
npx webpack &&
mv dist/* "$root/themes/default/static/_common/lib/"
