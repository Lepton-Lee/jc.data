const path = require("path");

module.exports = {
  entry: ["./index.js"],
  output: {
    filename: "mf2.js",
    path: path.resolve(__dirname, "dist"),
    library: {
      name: "mf2",
      type: "umd",
    },
  },
  devtool: "source-map",
};
